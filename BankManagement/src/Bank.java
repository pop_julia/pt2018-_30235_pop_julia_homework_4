import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class Bank implements BankProc, Serializable{

    private HashMap<Person, HashSet<Account>> map;

    private static final long serialVersionUID = 1L;

    public Bank(){
        map = new LinkedHashMap<>();

    }

    public HashMap<Person, HashSet<Account>> getMap() {
        return map;
    }

    public void setMap(HashMap<Person, HashSet<Account>> map) {
        this.map = map;
    }
    
    public void addStakeholderAssociatedAccount (Person person, Account account){
    	assert isWellFormed();
    	assert person != null && map.containsKey(person);
    	assert account != null;
    	
    	int sizePre = map.get(person).size();
    	map.get(person).add(account);
    	map.put(person, map.get(person));
    	
    	assert map.get(person).size() == sizePre + 1;
    	
    }
    public void deleteStakeholderAssociatedAccount(Person person, Account account){
    	assert isWellFormed();
    	assert person != null && map.containsKey(person);
    	assert account != null;
    	
    	int sizePre = map.get(person).size();
    	map.get(person).remove(account);
    	map.put(person, map.get(person));
    	
    	assert map.get(person).size() == sizePre + 1;
    	
    }

    @Override
    public void addStakeholder(Person person) {
        assert isWellFormed();
        assert person != null;

        int sizePre = map.size();
        map.put(person, new HashSet<>());

        assert sizePre + 1 == map.size();
        

    }

    @Override
    public void removeStakeholder(Person person) {
        assert isWellFormed();
        assert person != null;
        assert map.containsKey(person);

        int sizePre = map.size();
        map.remove(person);

        assert !map.containsKey(person);
        assert sizePre - 1 == map.size();
        

    }

    @Override
    public String generateReport(Person person) {
        assert isWellFormed();
        assert person != null;

        int sizePre = map.get(person).size();
        String string = null;
        for(Account account : map.get(person)){
            string = account.getId() + " " + account.getAmount() + "\n";
            System.out.println(string);
        }

        assert sizePre == map.get(person).size();
       
        return string;

    }

    @Override
    public void readAccountsData(Person person, int id) {
        assert isWellFormed();
        assert person != null;
        assert id >= 0;

        int sizePre = map.get(person).size();
        Iterator it = map.entrySet().iterator();
        while(it.hasNext()){
        	Map.Entry pair =(Map.Entry)it.next();
        	System.out.println(pair.getKey() + " = " + pair.getValue());
        	it.remove();
        	
        }
        

        System.out.println("Stakeholder name: " + person.getName() +  "with id" +
        id);

        assert sizePre == map.get(person).size();
        assert isWellFormed();

    }

    public void serialize(){
        try(FileOutputStream outputStream = new  FileOutputStream("bank.txt");
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(bufferedOutputStream)){

            objectOutputStream.writeObject(this);
            System.out.println("Serialized successfully in bank.txt");

        } catch (Exception e){
                e.printStackTrace();
        }
    }
    public Bank deserialize(){
        Bank bank = null;
        try(FileInputStream fileInputStream = new FileInputStream("bank.txt");
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            ObjectInputStream objectInputStream = new ObjectInputStream(bufferedInputStream)){

            bank = (Bank)objectInputStream.readObject();

        } catch (Exception e){
                e.printStackTrace();
        }
        System.out.println("Deserialize successfully from bank.txt");
        return bank;
    }

    private boolean isWellFormed(){
        for(Map.Entry<Person, HashSet<Account>> entry : map.entrySet()){
            if(entry.getKey() == null) {
                return false;
            }
        }
        return true;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((map == null) ? 0 : map.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bank other = (Bank) obj;
		if (map == null) {
			if (other.map != null)
				return false;
		} else if (!map.equals(other.map))
			return false;
		return true;
	}


}
