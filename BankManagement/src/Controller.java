import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.HashSet;


import javax.swing.JOptionPane;
import javax.swing.JTable;


public class Controller {
	private View view;
	private Bank bank = new Bank();
	private Person person;
	
	public Controller(View view) {
		this.view = view;
		bank = bank.deserialize();

		view.addAddPerson(new AddPerson());
		view.addAddPersonFrame(new AddPersonFrame());
		view.addRemovePerson(new RemovePerson());
		view.addRemovePersonFrame(new RemovePersonFrame());
		view.addPersonAccounts(new PersonAccounts());
		view.addLogin(new LogIn());
		view.addAddAmount(new AddAmount());
		view.addWithdrawAmount(new WithdrawAmount());
		view.addAddAccount(new AddAccount());
		view.addRemoveAccount(new RemoveAccount());
		populateTablePersons(bank.getMap());
	}
	
	public void populateTablePersons(HashMap<Person, HashSet<Account>> bank) {
		String[] columns = { "Name", "Accounts"};
		Object[][] data = new Object[30][3];
		Object[] persons = bank.keySet().toArray();
		for (int i = 0; i < persons.length; i++) {
			data[i][0] = ((Person) persons[i]).getName();
			data[i][1] = bank.get((Person) persons[i]).size();
			System.out.println(data[i][0] + " " + data[i][1]);
		}
		JTable tablePersons = new JTable(data, columns);
		view.setPersons(tablePersons);
		view.setTablePersons(tablePersons);
	}
	
	public void populateTableAccounts(HashSet<Account> accounts) {
		String[] columns = { "ID", "Stakeholder Name", "Amount", "Card Type"};
		Object[][] data = new Object[30][4];
		Object[] account = accounts.toArray();
		for (int i = 0; i < account.length; i++) {
			data[i][0] = ((Account) account[i]).getId();
			data[i][1] = ((Account) account[i]).getPerson().getName();
			data[i][2] = ((Account) account[i]).getAmount();
			data[i][3] = ((Account) account[i]).getName();
			System.out.println(data[i][0] + " " + data[i][1]);
		}
		JTable tableAccounts = new JTable(data, columns);
		view.setAccounts(tableAccounts);
		view.setTableAccounts(tableAccounts);
	}
	
	public class AddPerson implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			view.initializeAddPerson();
			view.getFrameAddPerson().setVisible(true);
		}
	}
	
	public class AddPersonFrame implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String name = view.getAddPersonNameTF().getText();
			Person person = new Person(name);
			try (FileInputStream fis = new FileInputStream("bank.txt");
					BufferedInputStream bis = new BufferedInputStream(fis);
				    ObjectInputStream ois = new ObjectInputStream(bis)){
				
				
			    bank.setMap(((Bank) ois.readObject()).getMap());

			    
	        }
	        catch(Exception ex) {
	        	ex.printStackTrace();
	        } 
			bank.addStakeholder(person);
			populateTablePersons(bank.getMap());
			bank.serialize();
		}
	}
	
	public class RemovePerson implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			view.initializeRemovePerson();
			view.getFrameRemovePerson().setVisible(true);
		}
	}
	
	public class RemovePersonFrame implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String name = view.getRemovePersonNameTF().getText();
			Person person = new Person(name);
			try (FileInputStream fis = new FileInputStream("bank.txt");
					BufferedInputStream bis = new BufferedInputStream(fis);
				    ObjectInputStream ois = new ObjectInputStream(bis)){
				
				
			    bank.setMap(((Bank) ois.readObject()).getMap());

			     
	        }
	        catch(Exception ex) {
	        	ex.printStackTrace();
	        } 
			bank.removeStakeholder(person);
			populateTablePersons(bank.getMap());
			bank.serialize();
		}
	}
	
	public class PersonAccounts implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			view.initializeFrameLogin();
			view.getFrameLogin().setVisible(true);
		}
	}
	
	public class LogIn implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String name = view.getNameLG().getText();
			try (FileInputStream fis = new FileInputStream("bank.txt");
				BufferedInputStream bis = new BufferedInputStream(fis);
			    ObjectInputStream ois = new ObjectInputStream(bis)){
				
			    bank.setMap(((Bank) ois.readObject()).getMap());

			    
	        }
	        catch(Exception ex) {
	        	ex.printStackTrace();
	        } 
			for (Person p: bank.getMap().keySet()) {
				if(p.getName().equals(name)) {
					person = p;
					view.getFrameLogin().setVisible(false);
					view.initializeAccountsFrame();
					populateTableAccounts(bank.getMap().get(person));
					view.getFrameAccounts().setVisible(true);
					return;
				}
			}
			
		}
	}
	
	public class AddAmount implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try (FileInputStream fis = new FileInputStream("bank.txt");
					BufferedInputStream bis = new BufferedInputStream(fis);
				    ObjectInputStream ois = new ObjectInputStream(bis)){
				
			    bank.setMap(((Bank) ois.readObject()).getMap());

			}
	        catch(Exception ex) {
	        	ex.getMessage();
	        } 
			int idAccount;
			double amount;
			try {
				idAccount = Integer.parseInt(view.getAccountIdTF().getText());
				amount = Double.parseDouble(view.getMoneyTF().getText());
				
				HashSet<Account> accounts = bank.getMap().get(person);
				for (Account a: accounts) {
					if (a.getId() == idAccount) {
						a.depositAmount(amount);
						//
						bank.getMap().get(person).add(a);
						bank.serialize();
						populateTableAccounts(accounts);
					}
				}
			} catch (Exception ex) {
				ex.getMessage();
			}
			
			
		}
	
	}
	
	public class WithdrawAmount implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try (FileInputStream fis = new FileInputStream("bank.txt");
					BufferedInputStream bis = new BufferedInputStream(fis);
				    ObjectInputStream ois = new ObjectInputStream(bis)){
			
				 bank.setMap(((Bank) ois.readObject()).getMap());
	        }
	        catch(Exception ex) {
	        	ex.getMessage();
	        } 
			int idAccount;
			double amount;
			try {
				idAccount = Integer.parseInt(view.getAccountIdTF().getText());
				amount = Double.parseDouble(view.getMoneyTF().getText());
				
				HashSet<Account> accounts = bank.getMap().get(person);
				for (Account account: accounts) {
					if (account.getId() == idAccount) {
						account.withdrawalAmount(amount);
						bank.getMap().get(person).add(account);
						bank.serialize();
						populateTableAccounts(accounts);
					}
				}
			} catch (Exception ex) {
				ex.getMessage();
			}
			
			
		}
	
	}
	
	public class AddAccount implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try (FileInputStream fis = new FileInputStream("bank.txt");
					BufferedInputStream bis = new BufferedInputStream(fis);
				    ObjectInputStream ois = new ObjectInputStream(bis)){
				
			
			    bank.setMap(((Bank) ois.readObject()).getMap());

			     
	        }
	        catch(Exception ex) {
	        	ex.printStackTrace();
	        } 
			int idAccount;
			double amount;
			String type;
			try {
				idAccount = Integer.parseInt(view.getAccountIdTF().getText());
				amount = Double.parseDouble(view.getMoneyTF().getText());
				type = view.getTypeTF().getText();
				
				if (type.equals("Saving Account")) {
					SavingAccount account = new SavingAccount(idAccount, amount, person);
					bank.addStakeholderAssociatedAccount(person, account);
	
	        		
	        		bank.serialize();
	        		populateTableAccounts(bank.getMap().get(person));
	        		populateTablePersons(bank.getMap());
	        	}
	        	else if (type.equals("Spending Account")) {
	        		SpendingAccount account = new SpendingAccount(idAccount, amount, person);
	        		bank.addStakeholderAssociatedAccount(person, account);
	        		bank.serialize();
	        		populateTableAccounts(bank.getMap().get(person));
	        		populateTablePersons(bank.getMap());
	        	}
	        
				
			} catch (Exception ex) {
				ex.getMessage();
			}
			
		}
	}
	
	public class RemoveAccount implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int idAccount;
			try(FileInputStream fis = new FileInputStream("bank.txt");
					BufferedInputStream bis = new BufferedInputStream(fis);
				    ObjectInputStream ois = new ObjectInputStream(bis)){
			    bank.setMap(((Bank) ois.readObject()).getMap());

			    
	        }
	        catch(Exception ex) {
	        	ex.printStackTrace();
	        } 
			try {
				idAccount = Integer.parseInt(view.getAccountIdTF().getText());
				Account account = null;
				for (Account a: bank.getMap().get(person)) {
					if (a.getId() == idAccount) {
						account = a;
						break;
					}
				}
				if (account != null) {
					bank.deleteStakeholderAssociatedAccount(person, account);
					bank.serialize();
					populateTableAccounts(bank.getMap().get(person));
					populateTablePersons(bank.getMap());
			
				
			}
				} catch (Exception ex) {
				ex.getMessage();
			}
			
		
	
	}
		}
	}
	
