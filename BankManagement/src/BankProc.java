public interface BankProc {
    /**
     * @pre person != null
     * @pre collection +1
     * @post map.size() == map.size() pre+1
     * @param person
     */
    void addStakeholder(Person person);

    /**
     * @pre person!=null && map.containsKey(person)
     * @post map.size() == map.size() pre-1
     * @param person
     */
    void removeStakeholder(Person person);

    /**
     * @pre person != null
     * @post map.get(person).size() == map.get(person).size() pre
     * @param person
     * @return
     */
    String generateReport(Person person);

    /**
     * @pre person != null && id >= 0
     * @post map.get(person).size() == map.get(person).map.size() pre
     * @param person
     * @param id
     */
    void readAccountsData(Person person, int id);
}
