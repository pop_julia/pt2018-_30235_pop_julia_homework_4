import java.io.Serializable;

public abstract class Account implements Serializable{
    private int id;
    private double amount;
    private Person person;

    private static final long serialVersionUID = 1L;

    public Account(int id, double amount, Person person) {
        this.id = id;
        this.amount = amount;
        this.person = person;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public abstract String getName();
    public abstract void depositAmount(double amount);
    public abstract void withdrawalAmount(double amount);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (id != account.id) return false;
        if (Double.compare(account.amount, amount) != 0) return false;
        return person != null ? person.equals(account.person) : account.person == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        temp = Double.doubleToLongBits(amount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (person != null ? person.hashCode() : 0);
        return result;
    }
}
