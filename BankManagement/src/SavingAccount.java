
public class SavingAccount extends Account {

    private final String name = "Saving Account";

    private static final long serialVersionUID = 1L;

    public SavingAccount(int id, double amount, Person person) {
        super(id, amount, person);
    }

    @Override
    public void depositAmount(double amount) {
    	if(amount > 2000){
        this.setAmount((this.getAmount() + amount - (amount * 0.10)));
        } else {
        	System.out.println("The minimun amount for deposit in the Saving Account is : 2000");
        }
    }

    @Override
    public void withdrawalAmount(double amount) {
    	if(amount > 1000){
        this.setAmount((this.getAmount() - amount - (amount * 0.15)));
        } else {
        	System.out.println("The minimum amout for withdrawal in the Saving account is : 1000");
        }
    	
    }

    public String getName() {
        return name;
    }
}
