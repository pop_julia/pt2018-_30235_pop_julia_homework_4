
public class SpendingAccount extends Account{

    private final String name = "Spending Account";

    private static final long serialVersionUID= 1L;

    public SpendingAccount(int id, double amount, Person person) {
        super(id, amount, person);
    }

    @Override
    public void depositAmount(double amount) {
    	this.setAmount((this.getAmount() + amount - (amount * 0.01)));
    }

    @Override
    public void withdrawalAmount(double amount) {
    	this.setAmount((this.getAmount() - amount - (amount * 0.02)));
    }

    public String getName() {
        return name;
    }
}
