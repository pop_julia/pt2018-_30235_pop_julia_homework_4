import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;

public class View {

	private JFrame frame;
	private JTable persons;
	private JTable accounts;
	
	protected JPanel panel = new JPanel();
	protected JScrollPane scrollPane1 = new JScrollPane();
	private final JButton btnAddPerson = new JButton("Add Stakeholder");
	private final JButton btnRemovePerson = new JButton("Remove Stakeholder");
	private final JButton btnPersonAccounts = new JButton("Stakeholder Accounts");
	

	private JFrame frameAddPerson;
	private JTextField addPersonNameTF;
	private JButton btnAdd = new JButton("ADD");
	

	private JFrame frameRemovePerson;
	private JTextField removePersonNameTF;
	JButton btnRemove = new JButton("REMOVE");
	

	private JFrame frameAccounts;
	
	protected JPanel panel2 = new JPanel();
	protected JScrollPane scrollPane2 = new JScrollPane();
	private final JButton btnAddAmount = new JButton("Add Money");
	private final JButton btnWithdrawAmount = new JButton("Withdraw Money");
	private final JLabel lblAccountId = new JLabel("Account ID:");
	private final JTextField accountIdTF = new JTextField();
	private final JTextField moneyTF = new JTextField();
	private final JLabel lblSum = new JLabel("Sum:");
	private final JButton btnAddAccount = new JButton("Add Account");
	private final JButton btnRemoveAccount = new JButton("Remove Account");
	private final JLabel lblType = new JLabel("Type:");
	private final JTextField typeTF = new JTextField();
	

	private JFrame frameLogin;
	private JTextField nameLG;
	
	private JButton btnOk = new JButton("OK");


	/**
	 * Create the application.
	 */


	public void setTablePersons(JTable tablePersons) {
		scrollPane1.setViewportView(tablePersons);
	}
	
	public void setTableAccounts(JTable tableAccounts) {
		scrollPane2.setViewportView(tableAccounts);
	}

	
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 795, 444);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		scrollPane1.setBounds(40, 11, 601, 230);
		panel.add(scrollPane1);
		btnAddPerson.setBackground(SystemColor.controlHighlight);
		btnAddPerson.setBounds(31, 293, 140, 38);
		
		panel.add(btnAddPerson);
		btnRemovePerson.setBackground(SystemColor.controlHighlight);
		btnRemovePerson.setBounds(265, 293, 140, 38);
		
		panel.add(btnRemovePerson);
		btnPersonAccounts.setBackground(SystemColor.controlHighlight);
		btnPersonAccounts.setBounds(475, 293, 140, 38);
		
		panel.add(btnPersonAccounts);
	}
	
	public void initializeAddPerson() {
		frameAddPerson = new JFrame();
		frameAddPerson.setBounds(100, 100, 310, 223);
		frameAddPerson.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(50, 63, 76, 14);
		
		addPersonNameTF = new JTextField();
		addPersonNameTF.setBounds(136, 60, 86, 20);
		addPersonNameTF.setColumns(10);

		
		JLabel lblAddNewPerson = new JLabel("Add Stakeholder");
		lblAddNewPerson.setBounds(88, 22, 120, 20);
		lblAddNewPerson.setFont(new Font("Geometric Shapes", Font.BOLD, 17));
		
		btnAdd.setBounds(88, 136, 102, 23);
		frameAddPerson.getContentPane().setLayout(null);
		frameAddPerson.getContentPane().add(lblName);
		frameAddPerson.getContentPane().add(addPersonNameTF);
		frameAddPerson.getContentPane().add(lblAddNewPerson);
		frameAddPerson.getContentPane().add(btnAdd);
	}
	
	public void initializeRemovePerson() {
		frameRemovePerson = new JFrame();
		frameRemovePerson.setBounds(100, 100, 310, 223);
		frameRemovePerson.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(50, 63, 76, 14);
		
		
		removePersonNameTF = new JTextField();
		removePersonNameTF.setBounds(136, 60, 86, 20);
		removePersonNameTF.setColumns(10);
		
	
		JLabel lblRemovePerson = new JLabel("Delete Stakeholder");
		lblRemovePerson.setBounds(88, 22, 120, 20);
		lblRemovePerson.setFont(new Font("Geometric Shapes", Font.BOLD, 17));
		
		
		btnRemove.setBounds(90, 140, 104, 25);
		frameRemovePerson.getContentPane().setLayout(null);
		frameRemovePerson.getContentPane().add(lblName);
		frameRemovePerson.getContentPane().add(removePersonNameTF);
		frameRemovePerson.getContentPane().add(lblRemovePerson);
		frameRemovePerson.getContentPane().add(btnRemove);
	}
	
	public void initializeAccountsFrame() {
		typeTF.setBounds(140, 293, 88, 22);
		typeTF.setColumns(10);
		moneyTF.setBounds(140, 264, 88, 22);
		moneyTF.setColumns(10);
		accountIdTF.setBounds(140, 234, 88, 22);
		accountIdTF.setColumns(10);
		frameAccounts = new JFrame();
		frameAccounts.setBounds(110, 110, 500, 460);
		frameAccounts.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frameAccounts.setVisible(true);
		
		frameAccounts.getContentPane().add(panel2, BorderLayout.CENTER);
		panel2.setLayout(null);
		
		scrollPane2.setBounds(40, 50, 410, 165);
		panel2.add(scrollPane2);
		btnAddAmount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAddAmount.setBackground(SystemColor.controlHighlight);
		btnAddAmount.setBounds(278, 210, 145, 40);
		
		panel2.add(btnAddAmount);
		btnWithdrawAmount.setBackground(SystemColor.controlHighlight);
		btnWithdrawAmount.setBounds(278, 265, 145, 40);
		
		panel2.add(btnWithdrawAmount);
		lblAccountId.setBounds(55, 240, 78, 16);

		
		
	}
	
	public void initializeFrameLogin() {
		frameLogin = new JFrame();
		frameLogin.setBounds(110, 110, 310, 180);
		frameLogin.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frameLogin.getContentPane().setLayout(null);
		
		JLabel lblName = new JLabel("Stakeholder name: ");
		lblName.setBounds(110, 70, 130, 60);
		frameLogin.getContentPane().add(lblName);
		
		
		nameLG = new JTextField();
		nameLG.setBounds(140, 30, 90,30);
		frameLogin.getContentPane().add(nameLG);
		nameLG.setColumns(10);
	
		
		btnOk.setBounds(100, 99, 96, 33);
		frameLogin.getContentPane().add(btnOk);
	}
	
}
